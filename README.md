# Udacity Fundamentals of Machine Learning and Artificial Inteligence Nanodegree

This repository will contains my Udacity projects in Fundamentals to Machine Learning and Artifical Inteligence Nanodegree.

# Why have I choose this Nanodegree?

I am a very curious person, meaning that I am always looking for knowledge. Thus, it could not be different with this Nanodegree.
Since it is built to attend the e-learning approach and I can study it in any available time, it suits my current needs, to learn
more about Machine Learning, Data Science and Artifical Inteligence.

# My Goal and Objective with this Nanodegree:

Learn the fundamentals of Data Science and Machine Learning, preparing myself to be a future Data Scientist and Machine Learning Engineer.

# Overview of Udacity ML Nanodegree:

It covers the following items:

-  Python language with a data analysis approach
- Python libraries numpy, pandas, scikit, matplotlib and seaborn
- Practial Statistics
- Introduction to Machine Learning and its most common algorithm
- Neural Network (Gradient Descent, Trading NNN and Deep Learning with PyTorch)
- Linear Algebra

# How is the Nanodegree?

It uses the computer based training methodology and PBL - Project Based Learning. Videos are short, max 5 minutes each, and they are
thought to help student doing the proposed projects. For this Nanodegree, two projects are mandatory.

# Project 1 - Explore Bikeshare Chicago Data
[TBD]

- Add link to Project Overview
- Add link to Project solution #1 (Python, cvs and Matplotlib)
- Add link to Project solution #2 (Numpy)
- Add link to Project solution #3 (Pandas)

# Project 2 - Predict Housing prices in Boston
[TBD]

- Add link to Project Overview
- Add link to Project solution


# Mini project 1 - Investigate a dataset related to wines
[TBD]

- Add link to Project Overview
- Add link to Project solution (using all tools)


# Mini project 2 - Predict survival outcomes from 1912 Titanic disaster
[TBD]

- Add link to Project Overview
- Add link to Project solution (using all tools)

# Mini project 3 - Create an Image Classifier using Deep Learning and Neural Network
[TBD]

- Add link to Project Overview
- Add link to Project solution (using all tools)